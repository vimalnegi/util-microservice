'use strict';
var chai = require('chai');
var assert = chai.assert;
var should = chai.should();
const { download, makeThumb, patchJson, copyObject } = require('../utils');
const _ = require('lodash');
const uuidv1 = require('uuid/v1');
const fs = require('fs');
const { publicPath } = require('../config')


var json = {
    name: 'Vimal',
    age: '21'
};
var patches = [{ "op": "replace", "path": "/age", "value": "22" }];
var output = {
    "age": "22",
    "name": "Vimal"
};
var testImageUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS9pKhNdORp7GaKjJUTFPfZwZfURmKVFiK5dWKn8eJQM5J40HHlaw"

describe('Tests', function() {
    describe('the utilities', function() {
        it('should patch json', function(done) {
            patchJson(json, patches)
                .then(data => {
                    data.should.eql(output);
                    done()
                })
                .catch(done)
        });

        it('should download photo', function(done) {
            this.timeout(1000000);
            var uri = testImageUrl;
            var filePath = `public/${uuidv1()}.jpg`;
            download(uri, filePath)
                .then(() => {
                    fs.existsSync(filePath).should.equal(true);
                    return done();
                })
                .catch((err) => {
                    console.log('error:-', err);
                    (1).should.equal(2);
                    return done(err)
                })
        });

        it('should thumb image', function(done) {
            this.timeout(1000000);
            var uri = testImageUrl;
            let host = '';
            makeThumb({ uri, host, publicPath })
                .then((response) => {
                    let filePath = `${publicPath}/${response.file}`;
                    fs.existsSync(filePath).should.equal(true);
                    return done();
                })
                .catch((err) => {
                    console.log('error:-', err)
                    return done(err);
                })
        })
    });
});