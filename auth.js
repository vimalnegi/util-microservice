const jwt = require('jsonwebtoken');
const SECRET = 'shhhh'
const status = require('http-status');


function signToken(id) {
    return jwt.sign({ id }, SECRET, {
        expiresIn: 60 * 60 * 10
    });
}

module.exports = {
    authenticate: (req, res) => {
        console.log('authenticate')
        console.log(req.body)
        let userId = req.body.userId;
        let password = req.body.password;
        authenticateUser(userId, password)
            .then(token => {
                console.log(token, 'token')
                res.status(status.OK).send({ success: true, token });
            })
            .catch(err => {
                res.status(status.UNAUTHORIZED).send({ success: false, err });
            })
    }
}

function authenticateUser(userId, password) {
    return new Promise((resolve, reject) => {
        if (userId == userId && password == password) {
            return resolve(signToken(userId))
        }
        return reject({ unauthorized: true })
    })
}