'use strict';


const jsonpatch = require('fast-json-patch');
var fs = require('fs');
var request = require('request');

const { thumb } = require('node-thumbnail');
const uuidv1 = require('uuid/v1');

var utils = {
    patchJson: function(json, patches) {
        json = copyObject(json);
        patches = copyObject(patches)
        return new Promise((resolve, reject) => {
            try {
                jsonpatch.applyPatch(json, patches, /*validate*/ true);
            } catch (err) {
                return reject({ patchError: true, err });
            }
            return resolve(json);
        })
    },
    makeThumb: function({ uri, host = "", publicPath }) {
        var uniqueName = uuidv1();
        var fileName = `${uniqueName}.jpg`;
        var thumbSuffix = '_thumb';
        var filePath = `public/${fileName}`;
        var thumbFileURI = host ? `${host}/${uniqueName}${thumbSuffix}.jpg` : `${uniqueName}${thumbSuffix}.jpg`
        return download(uri, filePath)
            .then(data => {
                return new Promise((resolve, reject) => {
                    thumb({
                        source: filePath, // could be a filename: dest/path/image.jpg
                        destination: publicPath,
                        suffix: thumbSuffix,
                        height: 50,
                        width: 50
                    }, function(files, err, stdout, stderr) {
                        if (err) {
                            return reject(err);
                        }
                        return resolve({ file: thumbFileURI })
                    });
                })
            })

    },
    download,
    copyObject
}

function copyObject(obj) {
    return JSON.parse(JSON.stringify(obj));
}

function download(uri, filename, callback) {
    return new Promise((resolve, reject) => {
        request.head(uri, function(err, res, body) {
            request(uri).pipe(fs.createWriteStream(filename)).on('close', () => resolve());
        });
    })

};

module.exports = utils;