var authController = require('./api/auth');
var util = require('./api/util')
const status = require('http-status');


function routes(app, options) {
    app.post('/auth', authController.authenticate);
    app.post('/jsonPatch', util.patch);
    app.post('/thumb', util.toThumb);

}

module.exports = routes;