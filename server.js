'use strict'
const express = require('express')
const morgan = require('morgan')
const helmet = require('helmet')
    // const movieAPI = require('../api/movies')
const status = require('http-status');
var routes = require('./routes');

const start = (options) => {
    console.log('about to start')
    return new Promise((resolve, reject) => {
        if (!options.port) {
            reject(new Error('The server must be started with an available port'))
        }
        const app = express()
        app.use(morgan('dev'));
        require('./config/express')(app);

        routes(app, options)
        app.use(helmet())
        app.use((err, req, res, next) => {
            reject(new Error('Something went wrong!, err:' + err))
            res.status(500).send('Something went wrong!')
        })

        // we add our API's to the express app
        // movieAPI(app, options)

        // finally we start the server, and return the newly created server 
        const server = app.listen(options.port, () => resolve(server))
    })
}

exports = module.exports = Object.assign({}, { start })