var bodyParser = require('body-parser');
var path = require('path');
var config = require('./index');
const express = require('express')



module.exports = function(app) {
    console.log('express.js')
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: false
    }));
    app.set('uploadPath', path.join(config.root, 'public'));
    app.use(express.static(config.root + "/public"));
}


/**
 * Express configuration
 */

// 'use strict';

// var express = require('express');
// var bodyParser = require('body-parser');
// // var path = require('path');
// // var config = require('./environment');
// // require('../util/cronj')

// module.exports = function(app) {
//     app.set('view engine', 'jade');
//     app.use(bodyParser.json());
//     app.use(bodyParser.urlencoded({
//         extended: false
//     }));

//     // app.use(express.static("../client"));
//     // app.use(express.static(config.root + "/public"));
//     // app.use(express.static(config.root + "/node_modules"));
//     // app.use(express.static(config.root + "/uploads"));
//     return require('http').Server(app);
// }