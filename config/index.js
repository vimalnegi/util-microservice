const path = require('path')

module.exports = {
    root: path.normalize(`${__dirname}/../`),
    publicPath: path.normalize(`${__dirname}/../public`)
}