const status = require('http-status');

const jsonpatch = require('fast-json-patch');
var fs = require('fs'),
    request = require('request');

var thumb = require('node-thumbnail').thumb;
var { publicPath } = require('../config');

var utils = require('../utils')


module.exports = {
    patch: (req, res) => {
        let json = req.body.json;
        let patches = req.body.patches;
        utils.patchJson(json, patches)
            .then(data => {
                res.status(status.OK).send({ success: true, data });
            })
            .catch(err => {
                res.status(status.INTERNAL_SERVER_ERROR).send({ success: false, err })
            })
    },
    toThumb: (req, res) => {
        let uri = req.body.uri;
        let host = req.headers.host;
        utils.makeThumb({ uri, host, publicPath })
            .then(data => {
                res.send({ success: true, data })
            })
            .catch(err => {
                res.status(status.INTERNAL_SERVER_ERROR).send({ success: false })
            })
    }
}